package co.pragra.qa.ispautomation.drivermanager;

import co.pragra.qa.ispautomation.config.FrameWorkConfig;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import javax.security.auth.login.Configuration;

public class DriverManager {
    private final Logger logger = LogManager.getLogger(DriverManager.class); //get logger
    private WebDriver driver;
    private static DriverManager instance;

    private DriverManager() {
        driver = init();
    }

    private WebDriver init() {
        if (FrameWorkConfig.getPropertyValue("testtype").equals("grid")) {
            //RemoteWebDriver webDriver = new RemoteWebDriver()
        } else {
            if (FrameWorkConfig.getPropertyValue("browser").equals("chrome")) {
                WebDriverManager.chromedriver().setup();
                logger.log(Level.INFO, "Found match for {} browser", FrameWorkConfig.getPropertyValue("browser"));//log info
                return new ChromeDriver();
            } else if (FrameWorkConfig.getPropertyValue("browser").equals("firefox")) {
                WebDriverManager.firefoxdriver().setup();
                return new FirefoxDriver();
            }
        }
        WebDriverManager.chromedriver().setup();
        logger.log(Level.INFO, "No match found for {} browser, returning default to CHROME", FrameWorkConfig.getPropertyValue("browser"));//log info
        return new ChromeDriver();
    }

    public static  WebDriver getWebDriver(){
        if(null==instance){
            instance = new DriverManager();
        }
        return instance.driver;
    }

}
