package co.pragra.qa.ispautomation.config;

import co.pragra.qa.ispautomation.exceptions.PropertyNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;


public class FrameWorkConfig {

    private static final Logger logger = LogManager.getLogger(FrameWorkConfig.class);
    private Properties properties;
    private static FrameWorkConfig instance;

    private FrameWorkConfig(){
        Path configPath = Paths.get("src/test/resources/framework.properties");
        logger.info("Reaching config from file from location  {} ", configPath.toString());

        try(InputStream stream = new FileInputStream(configPath.toFile())){
            properties = new Properties();
            properties.load(stream);
            logger.debug("Starting framework with following properties ");
            properties.forEach((k,v)->{
                logger.debug("Property name - {}  value - {}", k, v);
            });
        }catch (FileNotFoundException ex){
            logger.error("File - {} doesn't exists- please check", configPath.toString());
        }catch (IOException ex){
            logger.fatal("Something went wrong in reading file - Msg - {}", ex.getMessage());
            ex.printStackTrace();
        }
    }

    /**
     *  method will return the property as an object for an key
     *  if key is not found , it should throw exception propery not available
     * @param key
     * @return
     */
    public static Object getPropertyValue(String key) throws PropertyNotFoundException{
        logger.debug("Reading property for the key {} ", key);
            if(null == instance){
                instance =new FrameWorkConfig();
            }
            if(null==instance.properties.get(key)){
                logger.error("Couldn't locate value for the key {}", key);
                throw new PropertyNotFoundException("Couldn't find value for the property "+ key);
            }
        logger.debug("Found value for  key {}  - Value {}", key, instance.properties.get(key));
        return instance.properties.get(key);
    }
}
