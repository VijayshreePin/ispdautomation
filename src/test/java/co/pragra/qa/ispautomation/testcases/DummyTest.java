package co.pragra.qa.ispautomation.testcases;

import co.pragra.qa.ispautomation.config.FrameWorkConfig;
import co.pragra.qa.ispautomation.data.ExcelDataProvider;
import co.pragra.qa.ispautomation.drivermanager.DriverManager;
import co.pragra.qa.ispautomation.listeners.ScreenShotListener;
import co.pragra.qa.ispautomation.pages.homePage.LoginPage;
import co.pragra.qa.ispautomation.pages.homePage.UserDashboardPage;
import co.pragra.qa.ispautomation.pages.homePage.UserSignInPage;
import co.pragra.qa.ispautomation.pages.studentPage.StudentPage;
import co.pragra.qa.ispautomation.reports.HtmlReport;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.IExecutionListener;
import org.testng.annotations.*;
import javax.security.auth.login.Configuration;

import static co.pragra.qa.ispautomation.data.ExcelDataProvider.byEmail;

@Listeners({ScreenShotListener.class})
public class DummyTest {

    private WebDriver driver;
    private LoginPage loginPage;
    private LoginPage userDashboardPage;
    private LoginPage userSignInPage;
    private StudentPage studentPage;
    private final Logger logger = LogManager.getLogger(DummyTest.class);


    @BeforeSuite
    public void setUp(){
        driver = DriverManager.getWebDriver();
        driver.get(String.class.cast(FrameWorkConfig.getPropertyValue("site.url")));
        logger.info("entering application URL");
        logger.info("This is just a warning message");

    }

    @BeforeMethod
    public void setPages(){
        loginPage = new LoginPage(driver);
        userDashboardPage = new LoginPage(driver);
        userSignInPage = new LoginPage(driver);
        studentPage = new StudentPage(driver);
    }


    @Test
    public void LaunchBrowser() {
        WebDriver driver = DriverManager.getWebDriver();
        driver.get(String.class.cast(FrameWorkConfig.getPropertyValue("site.url")));
        ExtentTest testName = HtmlReport.getReportInstance().createTest("testName");
        testName.log(Status.FAIL, "Failed for a reason");
        Assert.fail(); //(to check that screenshot is being captured)
    }

    @Test(dataProvider = "excelData", dataProviderClass = ExcelDataProvider.class)
    public void testLogin(String TestCase, String email, String password){
        logger.info("******************************starting test case*****************************************");
        logger.info("******************************ISP Database URL Test*****************************************");
        loginPage.clickUserDashboard().userEmail(email).userPassword(password).clickUserLogin();
        studentPage.clickStudents();
        driver.manage().window().maximize();
        System.out.println(email);  //to print value from excel
        System.out.println(password);
        ExtentTest testName = HtmlReport.getReportInstance().createTest("testName");
        testName.log(Status.FAIL, "Failed for a reason");
        //Assert.fail(); //(to check that screenshot is being captured)
        logger.info("******************************ending test case*****************************************");
        logger.info("******************************ISP Database URL Test*****************************************");
    }


    @AfterSuite
    public void tearDown() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

       driver.quit();
    }

    public class ExecutionListener1 implements IExecutionListener {
        private long startTime;
        public ExecutionListener1() {

        }

        public void onExecutionStart() {
            this.startTime = System.currentTimeMillis();
            System.out.println("TestNG is going to start");
        }

        public void onExecutionFinish() {
            System.out.println("TestNG finished the task" + System.currentTimeMillis());
        }
    }


    @AfterClass
    public void flush(){
        HtmlReport.getReportInstance().flush();
    }

}

