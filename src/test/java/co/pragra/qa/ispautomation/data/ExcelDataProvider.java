package co.pragra.qa.ispautomation.data;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelDataProvider {
    private static String dataFileName="ispData.xlsx";
    private static String dataFileLocation="src/test/resources";
    public static String byEmail ="userEmail";
    private static String byPassword = "userPassword";


    @DataProvider(name="excelData")
    public static Iterator<Object[]>readExcel(){
        List<Object[]> excelData=new ArrayList<>();
        Path path = Paths.get(dataFileLocation,dataFileName);


    try(InputStream stream = new FileInputStream(path.toFile())){
        Workbook workbook = new XSSFWorkbook(stream);
        Sheet sheet = workbook.getSheet("Login");
        Iterator<Row>rowIterator = sheet.rowIterator();
        rowIterator.next();
            while(rowIterator.hasNext()){
                    Row row = rowIterator.next();

                    Iterator<Cell> cellIterator = row.cellIterator();
                    List<Object>cellData = new ArrayList<>();
                while(cellIterator.hasNext()){
                        Cell cell = cellIterator.next();
                    if (cell.getCellType()== CellType.STRING){
                        if(cell.getStringCellValue().isEmpty()||cell.getStringCellValue().equalsIgnoreCase("")){
                            continue;
                        }
                        cellData.add(cell.getStringCellValue());
                    }
                    if (cell.getCellType()==CellType.BOOLEAN){
                        cellData.add(cell.getBooleanCellValue());
                    }
                    if (cell.getCellType()==CellType.NUMERIC){
                        cellData.add(cell.getNumericCellValue());
                    }

                }
                    if(!cellData.isEmpty())
                    excelData.add(cellData.toArray());


            }
        }catch (FileNotFoundException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
        return excelData.iterator();

    }


}
