package co.pragra.qa.ispautomation.listeners;

import co.pragra.qa.ispautomation.drivermanager.DriverManager;
import co.pragra.qa.ispautomation.utils.FrameworkUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ScreenShotListener implements ITestListener {
    private final Logger logger = LogManager.getLogger(ScreenShotListener.class);
    @Override
    public void onTestStart(ITestResult result) {

    }

    @Override
    public void onTestSuccess(ITestResult result) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logger.log(Level.ERROR,"Test Failed{} - Doing Screenshot now",iTestResult.getName());
        FrameworkUtils.takeScreenshot(DriverManager.getWebDriver(),iTestResult.getName());
    }

    @Override
    public void onTestSkipped(ITestResult result) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {

    }

    @Override
    public void onStart(ITestContext context) {

    }

    @Override
    public void onFinish(ITestContext context) {

    }
}
