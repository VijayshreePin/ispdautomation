package co.pragra.qa.ispautomation.pages.studentPage;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudentPage {

    private WebDriver driver;
    private WebDriverWait wait ;

    @FindBy(xpath = "//a[@type='button' and @href='/user_login']")   //(css = "a[href=\"/user_login\"]")
    private WebElement userDashboard;

    @FindBy(xpath = "//input[@type='email']")
    private WebElement userEmail;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement userPassword;

    @FindBy(xpath ="//input[@value='Login']")
    private WebElement userLogin;

    @FindBy(xpath="//div//h3//i[@class='fa fa-users fs-28 wd-35']")//(css ="i[class='fa fa-user']")
    private WebElement students;

    @FindBy(xpath = "//div//span[@class='select2-arrow ui-select-toggle']")
    private WebElement sessionDropdownToggle;

    @FindBy(xpath = "//div//a[@class='btn btn-dark2 m-l-7 ng-scope' and contains(text(),'Student')]")
    private WebElement addNewStudentBtn;

    @FindBy(xpath = "//div/selectize[contains(@config,'StudentStatus')]/parent::*/child::div/child::div/input")
    private WebElement studentStatusDropdown;

    @FindBy(xpath = "//form//label[contains(text(),'Birth Country')]/parent::*/child::div/child::div/input")
    private WebElement studentSelectBirthCountry;

    @FindBy(xpath = "//div//label[contains(text(),'Birth Date')]/parent::*/child::*/p/span/button")
    private WebElement birthDatePicker;

    @FindBy(xpath = "//div/selectize[contains(@config,'Reason')]/parent::*/child::div/child::div/input")
    private WebElement studentReasonDropdown;

    @FindBy(xpath="//form//label[contains(text(),'Weeks')]")
    private WebElement weeksDropdown;


    public StudentPage(WebDriver driver) {
        // driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        this.driver = driver;
        wait = new WebDriverWait(driver,20);
        PageFactory.initElements(driver, this);
    }

    public StudentPage clickUserDashboard() {
        userDashboard = wait.until(ExpectedConditions.elementToBeClickable(userDashboard));
        userDashboard.click();
        return this;
    }

    public StudentPage userEmail(String email){
        userEmail  = wait.until(ExpectedConditions.visibilityOf(userEmail));
        userEmail.clear();
        userEmail.sendKeys(email);
        return this;
    }

    public StudentPage userPassword(String password){
        userPassword = wait.until(ExpectedConditions.visibilityOf(userPassword));
        userPassword.sendKeys(password);
        return this;
    }

    public StudentPage clickUserLogin() {
        userLogin.click();
        return this;
    }

    public StudentPage clickStudents(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement student =    wait.until(ExpectedConditions.elementToBeClickable(students));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();",students);
        return new StudentPage(driver);
    }

    public StudentPage selectSession(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement student = wait.until(ExpectedConditions.elementToBeClickable(sessionDropdownToggle));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", sessionDropdownToggle);
        return new StudentPage(driver);
    }

    public StudentPage clickAddNewStudentButton(){
        WebDriverWait wait = new WebDriverWait(driver, 200);
        WebElement student =    wait.until(ExpectedConditions.elementToBeClickable(addNewStudentBtn));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();",addNewStudentBtn);
        return new StudentPage(driver);
    }

    public StudentPage selectStudentStatusDropdown(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement student = wait.until(ExpectedConditions.elementToBeClickable(studentStatusDropdown));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", studentStatusDropdown);
        return new StudentPage(driver);
    }

    public StudentPage selectStudentReasonDropdown(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement student = wait.until(ExpectedConditions.elementToBeClickable(studentReasonDropdown));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", studentReasonDropdown);
        return new StudentPage(driver);
    }

//    public StudentPage studentFirstName() {
//        studentFirstName.sendKeys();
//        return this;
//
//    }

    public StudentPage selectWeeksDropdown(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement student = wait.until(ExpectedConditions.elementToBeClickable(weeksDropdown));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", weeksDropdown);
        return new StudentPage(driver);
    }

    public StudentPage selectBirthCountryDropdown(){
        WebDriverWait wait = new WebDriverWait(driver, 30);
        WebElement student = wait.until(ExpectedConditions.elementToBeClickable(studentSelectBirthCountry));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", studentSelectBirthCountry);
        return new StudentPage(driver);
    }

    public StudentPage selectBirthDatePicker(){
        WebDriverWait wait = new WebDriverWait(driver, 200);
        WebElement student =    wait.until(ExpectedConditions.elementToBeClickable(birthDatePicker));
        Actions actions = new Actions(driver);
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();",birthDatePicker);

        return new StudentPage(driver);
    }

}
