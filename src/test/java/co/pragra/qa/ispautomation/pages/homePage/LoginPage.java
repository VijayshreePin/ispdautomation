package co.pragra.qa.ispautomation.pages.homePage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//a[@type='button' and @href='/user_login']")
    private WebElement userDashboard;

    @FindBy(xpath = "//input[@type='email']")
    private WebElement userEmail;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement userPassword;

    @FindBy(xpath ="//input[@value='Login']")
    private WebElement userLogin;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver,50);
        PageFactory.initElements(driver, this);
    }
    public LoginPage clickUserDashboard() {
        userDashboard = wait.until(ExpectedConditions.elementToBeClickable(userDashboard));
        userDashboard.click();
        return this;
    }
    public LoginPage userEmail(String email){
        userEmail  = wait.until(ExpectedConditions.visibilityOf(userEmail));
        userEmail.clear();
        userEmail.sendKeys(email);
        return this;
    }
    public LoginPage userPassword(String password){
        userPassword = wait.until(ExpectedConditions.visibilityOf(userPassword));
        userPassword.sendKeys(password);
        return this;
    }
    public LoginPage clickUserLogin(){
        userLogin.click();
        return this;
    }

}
